package com.spring.root.entity;

import javax.persistence.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by seanc on 08/12/2016.
 */
@Entity
@Table(name="projects")
public class Project {


    @Id
    @Column( name = "projectID" )
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int projectID;

    @Column(name="userID")
    private int userID;

    @Column(name="projectName")
    private String projectName;

    @Lob
    @Column(name="description", length=512)
    private String description;

    @Column(name="goalAmount")
    private int goalAmount;

    @Column(name="endDate")
    private String endDate;

    @Column(name="embeded_video")
    private String embededVideo;

    @Transient
    private double amountPledged;

    @Column(name="isValid", columnDefinition="tinyint(1) default 1")
    public boolean isValid;

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }

    public Project(){};

    public Project(int projectID, int userID, String projectName, String description, int goalAmount, String endDate, String embededVideo){
        this.projectID = projectID;
        this.userID = userID;
        this.projectName = projectName;
        this.description = description;
        this.goalAmount = goalAmount;
        this.endDate = endDate;
        this.embededVideo = embededVideo;
    }

    public int getProjectID() {
        return projectID;
    }

    public void setProjectID(int projectID) {
        this.projectID = projectID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getGoalAmount() {
        return goalAmount;
    }

    public void setGoalAmount(int goalAmount) {
        this.goalAmount = goalAmount;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEmbededVideo(String embededVideo) {
        this.embededVideo = embededVideo;
    }

    public String getEmbededVideo() {
        return embededVideo;
    }

    public void setEndDate(Date endDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        this.endDate = sdf.format(endDate);
    }

    public double getAmountPledged() {
        return amountPledged;
    }

    public void setAmountPledged(double amountPledged) {
        this.amountPledged = amountPledged;
    }

    @Override
    public String toString(){
        String string;
        string = "Project ID: " + projectID + "\nUser ID: " + userID +
                "\nProject Name: " + projectName + "\nDescription: " + description +
                "\nGoal Amount: " + goalAmount + "\nEnd Date: " + endDate + "\n";
        return string;
    }
}

