package com.spring.root.entity;

/**
 * Created by C.I.T on 08-Dec-16.
 */

import javax.persistence.*;

@Entity
@Table(name="USERS")
public class User {

    @Id
    @Column( name = "id" )
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;


    @Column( name = "firstName" )
    private String firstName;

    @Column( name = "lastName" )
    private String lastName;

    @Column( name = "passwd" )
    private String passwd;

    @Column( name = "c_limit" )
    private double c_limit;


    /*Denotes that the variable is not an entry in that database*/

    @Transient
    private String fullName;

    protected User(){}

    public User(String firstName, String lastName, String passwd, double c_limit){
        this.firstName = firstName;
        this.lastName = lastName;
        this.passwd = passwd;
        this.c_limit = c_limit;
        this.fullName = firstName + ", " + lastName;
        System.out.println(this.id);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String name) {
        this.firstName = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String name) {
        this.lastName = name;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String name) {
        this.firstName = name;
    }

    public double getClimit() { return c_limit; }

    public void setClimit(double c_limit) {
        this.c_limit = c_limit;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName() {
        this.fullName = this.firstName + ", " + this.lastName;
    }

    @Override
    public String toString() {
        String out = "User [id=" + id + "\nFirst Name = " + firstName + "\nLast Name = " + lastName + "\nPassword = " + passwd + "\nCredit Limit = €" + c_limit;

        out += "]]";
        return out;
    }
}
