package com.spring.root.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.*;

/**
 * Created by seanc on 08/12/2016.
 */
@Entity
@Table(name="pledge")
public class Pledge {

    @Id
    @Column(name="pledgeId")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int pledgeID;

    @Column(name="userId")
    private int userID;

    @Column(name="projectId")
    private int projectID;

    @Column(name="amount")
    private double amount;

    public Pledge(){};

    public Pledge(int pledgeID, int userID, int projectID, double amount) {
        this.pledgeID = pledgeID;
        this.userID = userID;
        this.projectID = projectID;
        this.amount = amount;
    }

    public int getPledgeID() {
        return pledgeID;
    }

    public void setPledgeID(int pledgeID) {
        this.pledgeID = pledgeID;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }


    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getProjectID() {
        return projectID;
    }

    public void setProjectID(int projectID) {
        this.projectID = projectID;
    }

    //Create user and project setters and getters

    @Override
	public String toString(){

        return "Pledge ID: " + pledgeID + "\nAmount: " + amount
                + "\nProject ID: " + projectID;

    }

}
