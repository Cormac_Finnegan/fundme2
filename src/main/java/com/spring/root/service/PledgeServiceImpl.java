package com.spring.root.service;

import com.spring.root.entity.Pledge;
import com.spring.root.repository.PledgeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by seanc on 09/12/2016.
 */
@Service
public class PledgeServiceImpl implements  PledgeService {

    @Autowired
    private PledgeRepository pledgeRepository;

    @Override
    public Pledge get(int id) {
        for(Pledge p: pledgeRepository.findAll()){
            if(p.getPledgeID() == id){
                return p;
            }
        }
        return null;
    }

    @Override
    public void save(Pledge pledge) {
        pledgeRepository.save(pledge);
    }

    @Override
    public void remove(Pledge pledge) {
        pledgeRepository.delete(pledge);
    }

    @Override
    public List<Pledge> findAll() {
        return (List<Pledge>) pledgeRepository.findAll();
    }
}
