package com.spring.root.service;

import com.spring.root.entity.Pledge;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by seanc on 09/12/2016.
 */
@Service
public interface PledgeService {

    public Pledge get(int id);
    public void save(Pledge pledge);
    public void remove(Pledge pledge);
    public List<Pledge> findAll();
}
