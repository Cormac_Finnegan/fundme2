package com.spring.root.service;

import com.spring.root.entity.Pledge;
import com.spring.root.entity.User;
import com.spring.root.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by C.I.T on 08-Dec-16.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    PledgeService pledgeService;

    @Override
    public User get(int id) {
        User user;
        List<User> users = userRepository.findAll();
        for(int x = 0; x<users.size(); x++){
            if(id == users.get(x).getId()){
                return users.get(x);
            }
        }
        return userRepository.findOne(id);
    }
    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    public void add(User user) {

    }

    @Override
    public void update(User user) {

    }

    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }
    @Override
    public List<User> findAll() {
        return (List<User>) userRepository.findAll();
    }

    @Override
    public boolean checkCreditScore(User user, double pledgeAmount){
        List<Pledge> pledges = pledgeService.findAll();
        double amountPledged = pledgeAmount;
        for(Pledge p: pledges){
            if(p.getUserID()==user.getId()){
                amountPledged += p.getAmount();
            }
        }
        if(amountPledged >= user.getClimit()){
            return false;
        }
        return true;
    }
}
