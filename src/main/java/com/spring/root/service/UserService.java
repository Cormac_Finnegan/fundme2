package com.spring.root.service;

import com.spring.root.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by C.I.T on 08-Dec-16.
 */

@Service
public interface UserService {

    User get(int id);
    void save(User user);
    void add(User user);
    void update(User user);
    void delete(User user);
    List<User> findAll();
    boolean checkCreditScore(User user, double pledgeAmount);
}
