package com.spring.root.service;

import com.spring.root.entity.Pledge;
import com.spring.root.entity.Project;
import com.spring.root.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by seanc on 09/12/2016.
 */
@Service
public interface ProjectService {

    Project get(int id);
    Project getByName(String name);
    void save(Project project);
    void updateProjectDescription(int projectID, String description);
    void remove(Project project);
    List<Project> findAll();
    double getAmountPledged(int projectID);
    List<Pledge> getPledges(int projectID);
    boolean getIsValid(int projectID);
    void validateProjects();


}
