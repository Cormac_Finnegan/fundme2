package com.spring.root.service;

import com.spring.root.entity.Pledge;
import com.spring.root.entity.Project;
import com.spring.root.entity.User;
import com.spring.root.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by seanc on 09/12/2016.
 */
@Service
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ProjectRepository projectRepo;

    @Autowired
    PledgeService pledgeService;

    @Override
    public Project get(int id) {
        List<Project> projects = this.findAll();
        for(int x = 0; x<projects.size(); x++){
            if(id == projects.get(x).getProjectID()){
                return projects.get(x);
            }
        }
//        return projectRepo.get(id);
        return null;
    }

    @Override
    public Project getByName(String name){
        List<Project> projects = projectRepo.findAll();
        for(Project p: projects){
            if(p.getProjectName().equalsIgnoreCase(name)){
                return p;
            }
        }
        return null;
    }

    @Override
    public void save(Project project) {
        projectRepo.save(project);
    }

    @Override
    public void updateProjectDescription(int projectID, String description){
        Project p = projectRepo.findOne(projectID);

        p.setDescription(description);
        projectRepo.save(p);
    }

    @Override
    public void remove(Project project) {
        projectRepo.delete(project);
    }

    @Override
    public List<Project> findAll() {
        return projectRepo.findAll();
    }


    @Override
    public double getAmountPledged(int projectID){
        double amountPledged = 0;
        List<Pledge> pledges = pledgeService.findAll();
        for(Pledge p: pledges){
            if(p.getProjectID()== projectID){
                amountPledged+=p.getAmount();
            }
        }
        return amountPledged;
    }

    @Override
    public List<Pledge> getPledges(int projectID){
        List<Pledge> pledges = pledgeService.findAll();
        List<Pledge> projectPledges = pledgeService.findAll();
        for(Pledge p: pledges){
            if(p.getProjectID()!= projectID){
                projectPledges.remove(p);
            }
        }
        return projectPledges;
    }

    public boolean getIsValid(int id){
        return this.checkValidProject(id);
    }


    private boolean checkValidProject(int id){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date dateNow = new Date();

        String currentDate = sdf.format(dateNow);

        try {
            Date end = sdf.parse(this.get(id).getEndDate());
            if (dateNow.compareTo(end) > 0) {
                return false;
            } else if (dateNow.compareTo(end) < 0) {
                //this.isValid = true;
                double goalAmmount = this.get(id).getGoalAmount();
                double amountPledged = this.get(id).getAmountPledged();
                if(amountPledged < goalAmmount) {
                    return true;
                }else{
                    return false;
                }

            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void validateProjects(){
        SimpleDateFormat inputForm = new SimpleDateFormat("dd-MM-yyyy");
        List<Project> projects = projectRepo.findAll();
        Date todaysDate = Calendar.getInstance().getTime();
        String date = null;
        Date projectDate = null;

        for(Project p: projects){
            date = p.getEndDate();

            try {
                projectDate = inputForm.parse(date);
            }
            catch(Exception e){}

            p.setAmountPledged(getAmountPledged(p.getProjectID()));

            System.out.println(projectDate);
            System.out.println(todaysDate);

            if(todaysDate.after(projectDate)){
                System.out.println("\n\n\nAFTER\n\n\n");
            } else if (todaysDate.before(projectDate)) {
                System.out.println("\n\n\nBEFORE\n\n\n");
            }
            else{
                System.out.println("\n\n\nNEITHER\n\n\n");
            }

            if(todaysDate.after(projectDate) || p.getAmountPledged() >= p.getGoalAmount() ){
                Project old = projectRepo.findOne(p.getProjectID());
                old.setValid(false);
                projectRepo.save(old);
            }
        }
    }

}
