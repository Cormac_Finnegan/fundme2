package com.spring.root.form;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by seanc on 13/12/2016.
 */
public class LoginForm {

    @NotBlank
    @Length(min = 3, message = "First name must be at least 2 letters long")
    String firstName;

    @NotBlank
    @Length(min = 2, message = "Second name must be at least 2 letters long")
    String lastName;

    @NotBlank
    @Length(min = 3, message = "Password must be at least 6 characters long")
    String passwd;


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) { this.firstName = firstName; }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }





}

