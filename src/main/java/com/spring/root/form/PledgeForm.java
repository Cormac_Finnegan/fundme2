package com.spring.root.form;

import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by seanc on 19/12/2016.
 */
public class PledgeForm {


    @NotBlank
    String amount;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
