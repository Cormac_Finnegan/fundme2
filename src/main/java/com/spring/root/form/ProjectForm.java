package com.spring.root.form;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Min;
import java.util.Date;

/**
 * Created by seanc on 13/12/2016.
 */
public class ProjectForm {

    @NotBlank
    @Length(min = 3)
    String projectName;

    @NotBlank
    @Length(min = 4, message = "Description must be at least 40 characters")
    String description;

    String embedLink;

    @Min(value = 100, message = "Goal amount must be atleast €100")
    int goalAmount;

    @NotBlank
    String endDate;

    String embededVideo;

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmbedLink() {
        return embedLink;
    }

    public void setEmbedLink(String embedLink) {
        this.embedLink = embedLink;
    }

    public int getGoalAmount() {
        return goalAmount;
    }

    public void setGoalAmount(int goalAmount) {
        this.goalAmount = goalAmount;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEmbededVideo(){return embededVideo; }

    public void setEmbededVideo(String embededVideo) {
        this.embededVideo = embededVideo;
    }
}
