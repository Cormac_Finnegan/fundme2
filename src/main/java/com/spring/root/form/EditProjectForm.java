package com.spring.root.form;

import javax.validation.constraints.NotNull;

/**
 * Created by seanc on 20/12/2016.
 */
public class EditProjectForm {
    @NotNull
    String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
