package com.spring.root.form;

/**
 * Created by seanc on 19/12/2016.
 */
public class CancelPledge {

    int pledgeID;

    public int getPledgeID() {
        return pledgeID;
    }

    public void setPledgeID(int pledgeID) {
        this.pledgeID = pledgeID;
    }
}
