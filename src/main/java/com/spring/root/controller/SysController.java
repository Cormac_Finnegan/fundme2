package com.spring.root.controller;

import com.spring.root.LoginSystem;
import com.spring.root.entity.Pledge;
import com.spring.root.entity.Project;
import com.spring.root.entity.User;
import com.spring.root.form.*;
import com.spring.root.service.PledgeService;
import com.spring.root.service.ProjectService;
import com.spring.root.service.UserService;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by seanc on 13/12/2016.
 */
@Controller
public class SysController {


    @Autowired
    UserService userService;

    @Autowired
    ProjectService projectService;

    @Autowired
    PledgeService pledgeService;

    LoginSystem loggedIn = new LoginSystem();

    /*
    Redirect local:8080 to localhost:8080/home
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String defaultHome(Model model){
        return "redirect:/home";
    }

    /*
        HOMEPAGE
     */
    // Pass all projects to the home page
    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public ModelAndView home(Model model){
        model.addAttribute("loginValue", loggedIn.isLoggedIn());
        List<Project> projects = projectService.findAll();
        List<Project> validProjects = new ArrayList<>();
        int index = 0;

        for(Project project: projects){
            System.out.println(project.toString());
        };

        model.addAttribute("projectList",projects);
        return new ModelAndView("home");
    }

    // Pass all users to the user page
    @RequestMapping(value = "/user/list", method = RequestMethod.GET)
    public ModelAndView users(Model model){
        List<User> userList = userService.findAll();
        for(User user : userList){
            System.out.println(user.getId());
        }
        model.addAttribute("list", userList);
        return new ModelAndView("user/list");
    }

    /*
        LOGIN/LOGOUT
    */
    //Pass loginForm to login page
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model){
        model.addAttribute("loginForm", new LoginForm());
        return "login";
    }

    //Attempt login
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView tryLogin(Model model, @Valid LoginForm loginForm, BindingResult bindingResult)throws ParseException{
        if(bindingResult.hasErrors()){
            ModelAndView mav = new ModelAndView("login");
            return mav;
        }

        List<User> users = userService.findAll();
        for(User u: users){
            if(u.getFirstName().equalsIgnoreCase(loginForm.getFirstName()) &&
                    u.getLastName().equalsIgnoreCase(loginForm.getLastName()) &&
                    u.getPasswd().equalsIgnoreCase(loginForm.getPasswd())){
                loggedIn.setUser(u);
                loggedIn.setLoggedIn(true);
                System.out.println(loggedIn.isLoggedIn());
                return new ModelAndView("redirect:/home");
            }
        }
        return new ModelAndView("redirect:/login");

    }

    //Logout
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(Model model){
        loggedIn.setUser(null);
        loggedIn.setLoggedIn(false);
        return "redirect:/home";
    }


    /*
        ADD PROJECT
     */

    //Pass projectForm to add project page
    @RequestMapping(value = "/add_project", method = RequestMethod.GET)
    public String addProject(Model model){
        model.addAttribute("loginValue", loggedIn.isLoggedIn());
        model.addAttribute("projectForm", new ProjectForm());
        return "add_project";
    }

    //Attempt to add project
    @RequestMapping(value = "/add_project", method = RequestMethod.POST)
    public ModelAndView addProject(Model model, @Valid ProjectForm projectForm, BindingResult bindingResult)throws ParseException{
        if(bindingResult.hasErrors()){
            return new ModelAndView("add_project");
        }

        /*Check if someone is currently logged in */
        if(!loggedIn.isLoggedIn()){
            bindingResult.rejectValue("projectName", "projectName", "Please login before adding to a project");
            return new ModelAndView("add_project");
        }

        Project project = new Project();
        project.setProjectName(projectForm.getProjectName());
        project.setDescription(projectForm.getDescription());
        project.setEmbededVideo(projectForm.getEmbededVideo());
        project.setValid(true);
        project.setGoalAmount(projectForm.getGoalAmount());
        SimpleDateFormat inputForm = new SimpleDateFormat("yyyy-MM-dd");
        Date endDate = inputForm.parse(projectForm.getEndDate());
        SimpleDateFormat outputForm = new SimpleDateFormat("dd-MM-yyyy");
        project.setEndDate(outputForm.parse(outputForm.format(endDate)));

        projectService.save(project);


        return new ModelAndView("redirect:/home");
    }


    /*
        EDIT PROJECT
     */

    //Pass editProjectForm, project to be edited and project end date to edit project page
    @RequestMapping(value = "/edit_project/{projectID}", method = RequestMethod.GET)
    public String editProject(Model model, @PathVariable("projectID") int projectID){

        String projectEndDate = projectService.get(projectID).getEndDate();
        SimpleDateFormat inputForm = new SimpleDateFormat("dd-MM-yyyy");
        Date endDate = null;
        try {
            endDate = inputForm.parse(projectEndDate);
        }
        catch(Exception e){}

        SimpleDateFormat outputForm = new SimpleDateFormat("yyyy-MM-dd");

        try {
            endDate = outputForm.parse(outputForm.format(endDate));
        }
        catch(Exception e){}

        model.addAttribute("date", endDate);
        model.addAttribute("project", projectService.get(projectID));
        model.addAttribute("loginValue", loggedIn.isLoggedIn());
        model.addAttribute("editProjectForm", new EditProjectForm());
        return "edit_project";
    }

    //Attempt to edit project
    @RequestMapping(value = "/continue_edit/{projectID}", method = RequestMethod.POST)
    public ModelAndView continueEdit(Model model, @PathVariable("projectID") int projectID,
                                     @Valid EditProjectForm editProjectForm, BindingResult bindingResult)throws ParseException{

        if(bindingResult.hasErrors()){
            return new ModelAndView("redirect:/home");
        }


        String description = editProjectForm.getDescription();

        projectService.updateProjectDescription(projectID, description);


        return new ModelAndView("redirect:/view_project/" + projectID);
    }


    /*
        PROJECT PAGE
     */
    @RequestMapping(value = "/view_project/{projectID}", method = RequestMethod.GET)
    public ModelAndView view_project(Model model, @PathVariable("projectID") int id){
        model.addAttribute("loginValue", loggedIn.isLoggedIn());
        Project project = projectService.get(id);
        List<Pledge> projectPledges = projectService.getPledges(project.getProjectID());
        User creator = userService.get(project.getUserID());

        project.setAmountPledged(projectService.getAmountPledged(project.getProjectID()));
        creator.setFullName();


        model.addAttribute("project", project);
        model.addAttribute("pledges", projectPledges);
        model.addAttribute("userName", creator.getFullName());
        if(loggedIn.isLoggedIn()){
            model.addAttribute("user", loggedIn.getUser());
        }
        return new ModelAndView("project");
    }

    /*
        MAKE PLEDGE PAGE
     */
    //Pass pledgeForm to make pledge page
    @RequestMapping(value = "/make_pledge/{projectID}", method = RequestMethod.GET)
    public String makePledge(Model model, @PathVariable("projectID") int projectID){
        Project project = projectService.get(projectID);
        model.addAttribute("loginValue", loggedIn.isLoggedIn());
        model.addAttribute("project", project);
        model.addAttribute("pledgeForm", new PledgeForm());
        return "make_pledge";
    }

    //Attempt to make pledge
    @RequestMapping(value = "/make_project_pledge/{projectID}", method = RequestMethod.POST)
    public ModelAndView makePledge(Model model, @PathVariable("projectID") int projectID,
                                   @Valid PledgeForm pledgeForm, BindingResult bindingResult)throws ParseException{

        model.addAttribute("loginValue", loggedIn.isLoggedIn());
        User currentUser = loggedIn.getUser();

        double pledgeAmount;


        if(bindingResult.hasErrors()){
            return new ModelAndView("redirect:/make_pledge/"+projectID);
        }

        /* Try convert the amount to a double */
        try {
            pledgeAmount = Double.parseDouble(pledgeForm.getAmount());
        }
        catch (Exception e){
            bindingResult.rejectValue("amount", "amount", pledgeForm.getAmount()+ " is not a valid pledge");
            return new ModelAndView("redirect:/make_pledge/"+projectID);
        }

        /*Check if someone is currently logged in */
        if(!loggedIn.isLoggedIn()){
            bindingResult.rejectValue("amount", "amount", "Please login before pledging to a project");
            return new ModelAndView("redirect:/make_pledge/"+projectID);
        }

        if(currentUser != null && !userService.checkCreditScore(currentUser, pledgeAmount)){
            bindingResult.rejectValue("amount", "amount", "This pledge puts user over credit limit");
            return new ModelAndView("redirect:/make_pledge/"+projectID);
        }

        Pledge pledge = new Pledge();
        pledge.setProjectID(projectID);
        pledge.setAmount(pledgeAmount);
        pledge.setUserID(loggedIn.getUser().getId());
        pledgeService.save(pledge);
        return new ModelAndView("redirect:/home");
    }


    /*
        CANCEL PLEDGE
     */
    @RequestMapping(value = "/cancel_pledge/{pledgeID}", method = RequestMethod.GET)
    public ModelAndView cancelPledges(Model model, @PathVariable("pledgeID") int id){
        Pledge pledge = pledgeService.get(id);
        pledgeService.remove(pledge);
        return new ModelAndView("redirect:/home");
    }

    /*
        CANCEL PROJECT
     */
    @RequestMapping(value = "/cancel_project/{projectID}", method = RequestMethod.GET)
    public ModelAndView cancelProject(Model model, @PathVariable("projectID") int id){
        Project project= projectService.get(id);
        model.addAttribute("project", project);
        return new ModelAndView("cancel_project");
    }

    @RequestMapping(value = "/cancel_project/{projectID}/no", method = RequestMethod.GET)
    public ModelAndView doCancelProject(Model model, @PathVariable("projectID") int id){
        return new ModelAndView("redirect:/view_project/"+id);
    }

    @RequestMapping(value = "/cancel_project/{projectID}/yes", method = RequestMethod.GET)
    public ModelAndView dontCancelProject(Model model, @PathVariable("projectID") int id){
        Project project= projectService.get(id);
        projectService.remove(project);
        return new ModelAndView("redirect:/home");
    }


    @RequestMapping(value = "/close_project", method = RequestMethod.GET)
    public ModelAndView closeProjects(Model model){
        return new ModelAndView("close_project");
    }


    @RequestMapping(value = "/close_finished_projects", method = RequestMethod.GET)
    public ModelAndView closeFinishedProject(Model model){
        projectService.validateProjects();
        return new ModelAndView("redirect:/home");
    }

}
