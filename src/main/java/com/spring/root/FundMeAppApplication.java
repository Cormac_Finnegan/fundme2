package com.spring.root;


import com.spring.root.entity.Project;
import com.spring.root.entity.User;
import com.spring.root.repository.*;
import com.spring.root.service.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

import java.util.List;

@SpringBootApplication
public class FundMeAppApplication extends WebMvcConfigurerAdapter implements CommandLineRunner {

	@Autowired
	UserRepository userRepository;
	@Autowired
	UserService userService;
	@Autowired
	ProjectService projectService;

	@Autowired
	JdbcTemplate jdbcTemplate;



	public static void main(String[] args) {
		SpringApplication.run(FundMeAppApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {}


	private static TemplateEngine templateEngine;


	static {
		initializeTemplateEngine();
	}


	private static void initializeTemplateEngine() {

		ServletContextTemplateResolver templateResolver =
				new ServletContextTemplateResolver();
		// XHTML is the default mode, but we set it anyway for better understanding of code
		templateResolver.setTemplateMode("XHTML");
		// This will convert "home" to "/WEB-INF/templates/home.html"
		templateResolver.setPrefix("/WEB-INF/templates/");
		templateResolver.setSuffix(".html");
		// Template cache TTL=1h. If not set, entries would be cached until expelled by LRU
		templateResolver.setCacheTTLMs(3600000L);

		templateEngine = new TemplateEngine();
		templateEngine.setTemplateResolver(templateResolver);

	}


	//Needs user service first
	//Takes username and password as string and checks if they match any users in users table
//	public boolean logIn(String username, String password){
//		List<User> users = userService.getAll();
//		for(User user: users){
//			if(user.getFullName().equals(username) && user.getPasswd().equals(password))
// 				currentUser = user;
//				loggedIn = true;
//				return loggedIn;
//		}
//		return loggedIn;
//	}


}