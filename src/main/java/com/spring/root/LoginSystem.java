package com.spring.root;

import com.spring.root.entity.User;

/**
 * Created by seanc on 18/12/2016.
 */
public class LoginSystem {

    private User user;
    private boolean loggedIn = false;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }
}
