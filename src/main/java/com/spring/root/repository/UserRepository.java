package com.spring.root.repository;

import com.spring.root.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by C.I.T on 08-Dec-16.
 */
@Repository
public interface UserRepository extends CrudRepository<User, Integer> {


    //List<User> findByFullName(String fullName); // SELECT * FROM users WHERE fullName LIKE '%xxxxx%'
    @Override
    List<User> findAll();

}
