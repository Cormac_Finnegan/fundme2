package com.spring.root.repository;

import com.spring.root.entity.Project;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by seanc on 09/12/2016.
 */
@Repository
public interface ProjectRepository extends CrudRepository<Project, Integer> {

    @Override
	List<Project> findAll();



}
