INSERT INTO users VALUES (1, 100000.00, 'Cormac', 'Finnegan', 'one');
INSERT INTO users (c_limit, first_name,  last_name, passwd) VALUES ( 3000.00, 'Sean', 'Collins', 'two');
INSERT INTO users (c_limit, first_name, last_name, passwd) VALUES ( 1500.00, 'Kamil', 'Mudy', 'three');



INSERT INTO projects (projectID, userID, project_name, description, goal_amount, end_date, embeded_video) VALUES (1, 1, 'The AppDev Project','This project is based on our knowledge of various Application Development Framework elements such as a Spring MVC, Thymeleaf, Spring Data JPA, JUnit 4 + Mockito, Hibernate Validator or Spring Validator and H2 for an embedded databases. By pledging to this you are making a brighter future for 3 young budding software developers.', 5000, '12-03-2017', '<iframe width="560" height="315" src="https://www.youtube.com/embed/sTSA_sWGM44" frameborder="0" allowfullscreen="allowFullScreen"></iframe>');
INSERT INTO projects (userID, project_name, description, goal_amount, end_date, embeded_video) VALUES (2, 'NeverEnd','A new state of the art game built from Unreal Engine 4. In a very intense blink it and you will miss  it game, all you have to do is tap the screen to jump over logs... forever!', 20000, '01-01-2017', '<iframe width="560" height="315" src="https://www.youtube.com/embed/ZZ5LpwO-An4" frameborder="0" allowfullscreen="allowFullScreen"></iframe>');
INSERT INTO projects (userID, project_name, description, goal_amount, end_date, embeded_video) VALUES (3, 'A Dogs World', 'A Dogs world is a movie set in a dystopian future where dogs now rule the planet and keep humans as pets. A dogs world follows the supreme leader pup known as Rufus as he begins to feel empathy for his human pets.', 400, '25-12-2016', '<iframe width="560" height="315" src="https://www.youtube.com/embed/JpqiyFPdHZ4" frameborder="0" allowfullscreen="allowFullScreen"></iframe>');

INSERT INTO pledge (amount, project_id, user_id) VALUES (100, 1, 3);
INSERT INTO pledge (amount, project_id, user_id) VALUES (1000, 1, 3);
INSERT INTO pledge (amount, project_id, user_id) VALUES (200, 2, 1);
INSERT INTO pledge (amount, project_id, user_id) VALUES (2000, 2, 1);
INSERT INTO pledge (amount, project_id, user_id) VALUES (30, 3, 2);
INSERT INTO pledge (amount, project_id, user_id) VALUES (300, 3, 2);