package com.spring.root.repository;

import com.spring.root.AppConfig;
import com.spring.root.FundMeAppApplication;
import com.spring.root.service.UserService;
import com.spring.root.service.UserServiceImpl;
import junit.framework.Assert;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.spring.root.entity.User;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

/**
 * Created by C.I.T on 20-Dec-16.
 */

@ContextConfiguration(classes={FundMeAppApplication.class})
@RunWith(SpringJUnit4ClassRunner.class)
public class UserRepositoryTest {

    @Autowired
    ApplicationContext context;

    @Autowired
    UserRepository userRepo;


    //------------------------------------------------- find all
    @Test
    public void testFindAll() {
        List<User> userList = userRepo.findAll();
        Assert.assertNotNull(userList);
        for(User user : userList) {
            System.out.println(user.toString());
        }

    }

    //------------------------------------------------- find one
    @Test
    public void testGetSingleUser() {
        User user1 = userRepo.findOne(1);
        Assert.assertNotNull(user1);

    }

    //------------------------------------------------- change user name
    @Test
    public void testChangeUserName() {
        User user1 = userRepo.findOne(1);
        Assert.assertNotNull(user1);
        String testName = "NewName";
        user1.setFirstName(testName);
        org.junit.Assert.assertEquals(user1.getFirstName(), testName);
    }

    //------------------------------------------------- delete a user
    @Test
    public void testDeleteUser() {
        User userToBeDeleted = userRepo.findOne(1);
        Assert.assertNotNull(userToBeDeleted);
        userRepo.delete(userToBeDeleted);
        Assert.assertFalse(userRepo.exists(userToBeDeleted.getId()));

    }

    //------------------------------------------------- delete a user
    @Test
    public void testAddnewUser() {
        User userToBeAdded = new User("NewFirst", "Newlast", "testpass", 100.00);
        Assert.assertNotNull(userToBeAdded);
        userRepo.save(userToBeAdded);
        Assert.assertNotNull(userToBeAdded.getId());
    }


}
