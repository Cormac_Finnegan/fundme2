package com.spring.root.repository;

import com.spring.root.FundMeAppApplication;
import com.spring.root.entity.Project;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.List;

/**
 * Created by C.I.T on 20-Dec-16.
 */
@ContextConfiguration(classes={FundMeAppApplication.class})
@RunWith(SpringJUnit4ClassRunner.class)
public class ProjectRepositoryTest {

    @Autowired
    ApplicationContext context;

    @Autowired
    ProjectRepository projectRepo;


    //------------------------------------------------- find all
    @Test
    public void testFindAll() {
        List<Project> projectList = projectRepo.findAll();
        Assert.assertNotNull(projectList);
        for(Project project : projectList) {
            System.out.println(project.toString());
        }

    }

    //------------------------------------------------- find one
    @Test
    public void testGetSingleProject() {
        Project project1 = projectRepo.findOne(1);
        Assert.assertNotNull(project1);

    }

    //------------------------------------------------- change project name
    @Test
    public void testChangeProjectName() {
        Project project1 = projectRepo.findOne(1);
        Assert.assertNotNull(project1);
        String testName = "New Project Name";
        project1.setProjectName(testName);
        org.junit.Assert.assertEquals(project1.getProjectName(), testName);
    }

    //------------------------------------------------- delete a project
    @Test
    public void testDeleteProject() {
        Project projectToBeDeleted = projectRepo.findOne(1);
        Assert.assertNotNull(projectToBeDeleted);
        projectRepo.delete(projectToBeDeleted);
        Assert.assertFalse(projectRepo.exists(projectToBeDeleted.getProjectID()));

    }
}
