package com.spring.root.repository;

import com.spring.root.FundMeAppApplication;
import com.spring.root.entity.Pledge;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * Created by C.I.T on 20-Dec-16.
 */
@ContextConfiguration(classes={FundMeAppApplication.class})
@RunWith(SpringJUnit4ClassRunner.class)
public class PledgeRepositoryTest {


    @Autowired
    ApplicationContext context;

    @Autowired
    PledgeRepository pledgeRepo;


    //------------------------------------------------- find all
    @Test
    public void testFindAll() {
        List<Pledge> pledgeList = pledgeRepo.findAll();
        Assert.assertNotNull(pledgeList);
        for(Pledge pledge : pledgeList) {
            System.out.println(pledge.toString());
        }

    }

    //------------------------------------------------- find one
    @Test
    public void testGetSinglePledge() {
        Pledge pledge1 = pledgeRepo.findOne(1);
        Assert.assertNotNull(pledge1);

    }


    //------------------------------------------------- delete a pledge
    @Test
    public void testDeletePledge() {
        Pledge pledgeToBeDeleted = pledgeRepo.findOne(1);
        Assert.assertNotNull(pledgeToBeDeleted);
        pledgeRepo.delete(pledgeToBeDeleted);
        Assert.assertFalse(pledgeRepo.exists(pledgeToBeDeleted.getPledgeID()));

    }
    
}
